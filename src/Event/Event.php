<?php namespace Ayedev\Integration\Facebook\Event;

use Ayedev\Bot\Messenger\Impl\AbstractEvent;
use Ayedev\Integration\Facebook\Core\Entry;
use Ayedev\Integration\Facebook\Model\Message;
use Ayedev\Bot\Messenger\Traits\HasContextsTrait;
use Ayedev\Bot\Messenger\Traits\HasLocationTrait;

class Event extends AbstractEvent
{
    use HasLocationTrait, HasContextsTrait;

    /**
     * Event constructor.
     *
     * @param $sender
     * @param $recipient
     * @param $time
     * @param $page_id
     * @param mixed $entry
     */
    public function __construct( $sender, $recipient, $time, $page_id, Entry $entry )
    {
        //  Store Data
        $this->_sender = $sender;
        $this->_recipient = $recipient;
        $this->_page_id = $page_id;
        $this->_time = $time;
        $this->_entry = $entry;

        //  Read State
        $this->readState();
    }


    /**
     * Generate Entries
     *
     * @param $rawData
     * @param $pageID
     * @return Event
     */
    public static function generate( $rawData, $pageID )
    {
        //  Type
        $type = self::RAW_DATA;

        //  Payload for Entry
        $payload = $rawData;

        //  Check for Payloads
        if( isset( $rawData['message'] ) )
        {
            //  Set Payload
            $payload = $rawData['message'];

            //  Check
            if( isset( $rawData['message']['is_echo'] ) )
            {
                //  Set Type
                $type = self::MESSAGE_ECHO;
            }
            else if( isset( $rawData['message']['attachments'] ) && isset( $rawData['message']['attachments'][0] ) && $rawData['message']['attachments'][0]['type'] == 'location' )
            {
                //  Set Type
                $type = self::LOCATION_RECEIVED;
            }
            else
            {
                //  Set Type
                $type = self::MESSAGE_RECEIVED;
            }
        }
        else if( isset( $rawData['postback'] ) )
        {
            //  Set Type & Payload
            $type = self::MESSAGE_POSTBACK;
            $payload = $rawData['postback'];
        }
        else if( isset( $rawData['delivery'] ) )
        {
            //  Set Type & Payload
            $type = self::MESSAGE_DELIVERY;
            $payload = $rawData['delivery'];
        }
        else if( isset( $rawData['read'] ) )
        {
            //  Set Type & Payload
            $type = self::MESSAGE_READ;
            $payload = $rawData['read'];
        }

        //  Create Event
        return new static( $rawData['sender'], $rawData['recipient'], $rawData['timestamp'], $pageID, new Entry( $payload, $type ) );
    }

    /**
     * Get the Profile
     *
     * @return \Ayedev\Integration\Facebook\Model\UserProfile
     */
    public function getProfile()
    {
        //  Static
        static $_profile;
        if( !$_profile )    $_profile = mManager()->getMessenger()->getUserProfile( $this->getSenderID() );

        //  Return
        return $_profile;
    }

    /**
     * @inheritdoc
     */
    public function replyObject($message, $messageClass = null)
    {
        //  Check
        if( !$messageClass )    $messageClass = Message::class;

        //  Create Reply Message
        $reply = new $messageClass( $message );

        //  Return
        return $reply;
    }
}