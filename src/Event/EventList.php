<?php namespace Ayedev\Integration\Facebook\Event;

use Ayedev\Bot\Messenger\Impl\AbstractList;

class EventList extends AbstractList
{
    /** @var string $_id */
    private $_id;

    /** @var int $_id */
    private $_time;


    /**
     * EntryList constructor.
     *
     * @param $id
     * @param $time
     * @param array $list
     */
    public function __construct( $id, $time, $list = array() )
    {
        //  Store
        $this->_list = $list;
    }


    /**
     * Generate Entries
     *
     * @param $data
     * @return EventList
     */
    public static function generate( $data )
    {
        //  Entry List
        $list = new static( $data['id'], $data['time'] );

        //  Loop Each
        foreach( $data['messaging'] as $rawData )
        {
            //  Create Entry
            $list->add( Event::generate( $rawData, $data['id'] ) );
        }

        //  Return
        return $list;
    }

    /**
     * Get ID
     *
     * @return string
     */
    public function getID()
    {
        //  Return
        return $this->_id;
    }

    /**
     * Get Page ID
     *
     * @return string
     */
    public function getPageID()
    {
        //  Return
        return $this->getID();
    }

    /**
     * Get Time
     *
     * @return int
     */
    public function getTime()
    {
        //  Return
        return $this->_time;
    }
}