<?php namespace Ayedev\Integration\Facebook;

use Ayedev\Bot\Messenger\Core\ApiCall;
use Ayedev\Bot\Messenger\Impl\AbstractWebhook;

class Webhook extends AbstractWebhook
{
    /**
     * @inheritdoc
     */
    public function createApiCall()
    {
        //  Return
        return new ApiCall( null, null, 'https://graph.facebook.com', 'v2.6' );
    }

    /**
     * Validate Bot Callback
     *
     * @return bool
     */
    public function validateBotCallback()
    {
        //  Validate Hub Signature
        if( !$this->isValidHubSignature() ) return false;

        //  Get Decoded Body
        $decoded = $this->getDecodedBody();

        //  Get Object
        $object = isset($decoded['object']) ? $decoded['object'] : null;

        //  Get Entry
        $entry = isset($decoded['entry']) ? $decoded['entry'] : null;

        //  Return
        return $object === 'page' && null !== $entry;
    }

    /**
     * Generate Events
     *
     * @return array
     */
    protected function generateEvents()
    {
        //  Check
        if( !$this->_events )
        {
            //  Get Decoded Body
            $decodedBody = $this->getDecodedBody();

            //  Raw Entries
            $entries = ( isset( $decodedBody['entry'] ) ? $decodedBody['entry'] : array() );

            //  Clear
            $this->_events = array();

            //  Loop Each
            foreach( $entries as $entry )
            {
                //  Store Entry
                $this->_events[] = Event\EventList::generate( $entry );
            }
        }

        //  Return
        return $this->_events;
    }
}