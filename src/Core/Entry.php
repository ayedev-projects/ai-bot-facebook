<?php namespace Ayedev\Integration\Facebook\Core;

use Ayedev\Bot\Messenger\Impl\AbstractEntry;

class Entry extends AbstractEntry
{
    /**
     * Get ID
     *
     * @return string
     */
    public function getID()
    {
        //  Return
        return $this->getValue( 'id', $this->getValue( 'mid' ) );
    }

    /**
     * Get Sequence
     *
     * @return string
     */
    public function getSequence()
    {
        //  Return
        return $this->getValue( 'seq' );
    }

    /**
     * Get Text
     *
     * @return string
     */
    public function getText()
    {
        //  Return
        return $this->getValue( 'text' );
    }

    /**
     * Get Quick Reply
     *
     * @return array
     */
    public function getQuickReply()
    {
        //  Return
        return $this->getValue( 'quick_reply' );
    }

    /**
     * Get Quick Reply Payload
     *
     * @return array
     */
    public function getQuickReplyPayload()
    {
        //  Return
        return ( $this->isQuickReply() ? $this->getQuickReply()['payload'] : null );
    }

    /**
     * Get Attachments
     *
     * @return array
     */
    public function getAttachments()
    {
        //  Return
        return $this->getValue( 'attachments' );
    }

    /**
     * Get Location Data
     *
     * @return array
     */
    public function getLocation()
    {
        //  Return
        return ( $this->hasLocation() ? $this->getAttachments()[0] : null );
    }

    /**
     * Check has Quick Reply
     *
     * @return bool
     */
    public function isQuickReply()
    {
        //  Return
        return $this->hasValue( 'quick_reply' );
    }

    /**
     * Check has Attachments
     *
     * @return bool
     */
    public function hasAttachments()
    {
        //  Return
        return ( $this->hasValue( 'attachments' ) && sizeof( $this->getAttachments() ) > 0 );
    }

    /**
     * Check has Location
     *
     * @return bool
     */
    public function hasLocation()
    {
        //  Return
        return ( $this->hasAttachments() ? $this->getAttachments()[0]['type'] === 'location' : false );
    }

    /**
     * Get Postback Payload
     *
     * @return array
     */
    public function getPostbackPayload()
    {
        //  Return
        return $this->getValue( 'payload' );
    }

    /**
     * Get Postback Referral
     *
     * @return array
     */
    public function getPostbackReferral()
    {
        //  Return
        return $this->getValue( 'referral' );
    }
}