<?php namespace Ayedev\Integration\Facebook\Model;

use Ayedev\Bot\Messenger\Impl\AbstractMessage;

abstract class Button extends AbstractMessage
{
    //  Button Types
    const TYPE_POSTBACK = 'postback';
    const TYPE_WEB_URL = 'web_url';
    const TYPE_SHARE = 'element_share';

    /** @var array $_fillable */
    protected $_fillable = array( 'type' );


    /**
     * @param string $title
     *
     * @throws \InvalidArgumentException
     */
    public static function validateTitleSize($title)
    {
        if (mb_strlen($title) > 20) {
            throw new \InvalidArgumentException('The button title field should not exceed 20 characters.');
        }
    }

    /**
     * @param $payload
     *
     * @throws \InvalidArgumentException
     */
    public static function validatePayload($payload)
    {
        if (mb_strlen($payload) > 1000) {
            throw new \InvalidArgumentException(sprintf(
                    'Payload should not exceed 1000 characters.', $payload)
            );
        }
    }
}