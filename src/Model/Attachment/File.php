<?php namespace Ayedev\Integration\Facebook\Model\Attachment;

use Ayedev\Integration\Facebook\Model\Attachment;

class File extends Attachment
{
    /**
     * @var string|null
     */
    private $_path;

    /**
     * @var bool
     */
    private $_isRemoteFile;

    /**
     * @var null|resource
     */
    private $_stream;

    /**
     * @var array
     */
    protected $allowedExtensions = [];


    /**
     * File constructor.
     *
     * @param $filePath
     * @param array|Template|string $type
     */
    public function __construct( $filePath, $type = Attachment::TYPE_FILE )
    {
        //  Store Path
        $this->_path = $filePath;

        //  Local image is sent with a multipart request
        $payload = [];

        //  Check for Remote FIle
        if( $this->isRemoteFile() )
        {
            //  Create Payload
            $payload = [
                'url' => $this->_path
            ];
        }

        //  Validate Extension
        $this->validateExtension();

        //  Run Setup
        $this->setupAttachment( $type, $payload );
    }

    /**
     * @return string
     */
    public function getPath()
    {
        //  Return
        return $this->_path;
    }

    /**
     * Open File
     */
    public function open()
    {
        //  Check
        if( $this->_stream )    return;

        //  Check Remote
        if( $this->isRemoteFile() ) throw new \RuntimeException( 'A remote file can not be open' );

        //  Check is readable
        if( !is_readable( $this->_path ) )  throw new \InvalidArgumentException( sprintf( 'The file "%s" should be readable', $this->_path ) );

        //  Open Stream
        $this->_stream = fopen( $this->_path, 'r' );

        //  Check
        if( !$this->_stream )   throw new \InvalidArgumentException( sprintf( 'Unable to open the media "%s"', $this->_path ) );
    }

    /**
     * Get Stream
     *
     * @return null|resource
     */
    public function getFileStream()
    {
        //  Open Stream
        $this->open();

        //  Return
        return $this->_stream;
    }

    /**
     * Check for Remote File
     *
     * @return bool
     */
    public function isRemoteFile()
    {
        //  Check Resolved
        if( isset( $this->_isRemoteFile ) ) return $this->_isRemoteFile;

        //  Return
        return ( $this->_isRemoteFile = preg_match( '/^(https?|ftp):\/\/.*/', $this->_path ) === 1 );
    }

    /**
     * Check for File to Upload
     *
     * @return bool
     */
    public function hasFileToUpload()
    {
        //  Return
        return !$this->isRemoteFile();
    }

    /**
     * Validate Extension
     */
    private function validateExtension()
    {
        //  Check
        if( !empty( $this->allowedExtensions ) )
        {
            //  Get Extension
            $ext = pathinfo( $this->_path, PATHINFO_EXTENSION );

            //  Check
            if( empty( $ext ) ) return false;

            //  Check
            if( !in_array( $ext, $this->allowedExtensions ) )
            {
                //  Throw Exception
                throw new \InvalidArgumentException(
                    sprintf( '"%s" this file has not an allowed extension. Only %s extensions are supported', $this->_path, '["'. implode( '", "', $this->allowedExtensions ) .'"]' )
                );
            }
        }

        //  Return
        return true;
    }
}