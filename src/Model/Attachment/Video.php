<?php namespace Ayedev\Integration\Facebook\Model\Attachment;

use Ayedev\Integration\Facebook\Model\Attachment;

class Video extends File
{
    /**
     * Video constructor.
     *
     * @param $filePath
     */
    public function __construct( $filePath )
    {
        //  Run Parent
        parent::__construct( $filePath, Attachment::TYPE_VIDEO );
    }
}