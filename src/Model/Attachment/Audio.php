<?php namespace Ayedev\Integration\Facebook\Model\Attachment;

use Ayedev\Integration\Facebook\Model\Attachment;

class Audio extends File
{
    /**
     * Audio constructor.
     *
     * @param $filePath
     */
    public function __construct( $filePath )
    {
        //  Run Parent
        parent::__construct( $filePath, Attachment::TYPE_AUDIO );
    }
}