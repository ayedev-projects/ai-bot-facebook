<?php namespace Ayedev\Integration\Facebook\Model\Attachment;

use Ayedev\Integration\Facebook\Model\Attachment;

class Image extends File
{
    /** @var array $allowedExtensions */
    protected $allowedExtensions = ['jpg', 'gif', 'png'];


    /**
     * Image constructor.
     *
     * @param $filePath
     */
    public function __construct( $filePath )
    {
        //  Run Constructor
        parent::__construct( $filePath, Attachment::TYPE_IMAGE );
    }
}