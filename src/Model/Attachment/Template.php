<?php namespace Ayedev\Integration\Facebook\Model\Attachment;

use Ayedev\Bot\Messenger\Impl\AbstractMessage;

class Template extends AbstractMessage implements \JsonSerializable
{
    //  Types
    const TYPE_GENERIC = 'generic';
    const TYPE_BUTTON = 'button';
    const TYPE_LIST = 'list';

    /** @var array $_fillable */
    protected $_fillable = array( 'type', 'payload' );


    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[TEMPLATE]';
    }
}