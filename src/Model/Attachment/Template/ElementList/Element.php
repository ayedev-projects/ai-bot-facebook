<?php namespace Ayedev\Integration\Facebook\Model\Attachment\Template\ElementList;

use Ayedev\Integration\Facebook\Model\Attachment\Template\AbstractElement;
use Ayedev\Integration\Facebook\Model\Button;
use Ayedev\Integration\Facebook\Model\DefaultAction;

class Element extends AbstractElement
{
    /** @var array $_fillable */
    protected $_fillable = array( 'title', 'subtitle', 'image_url', 'buttons', 'default_action' );


    /**
     * Element constructor.
     *
     * @param $title
     * @param null $subtitle
     * @param null $imageUrl
     * @param Button $button
     * @param DefaultAction|null $defaultAction
     */
    public function __construct($title, $subtitle = null, $imageUrl = null, Button $button = null, DefaultAction $defaultAction = null)
    {
        //  Setup Element
        $this->setupElement( $title, $subtitle, $imageUrl );

        //  Set Buttons
        $this->setButtons( [ $button ] );

        //  Set Default Action
        $this->setDefaultAction( $defaultAction );
    }
}