<?php namespace Ayedev\Integration\Facebook\Model\Attachment\Template;

use Ayedev\Integration\Facebook\Model\Attachment\Template;

class Generic extends Template implements \JsonSerializable
{
    /** @var array $_fillable */
    protected $_fillable = array( 'template_type', 'elements' );


    /**
     * Generic constructor.
     *
     * @param array $elements
     *
     */
    public function __construct( array $elements )
    {
        //  Validate
        if( count( $elements ) > 10 )   throw new \InvalidArgumentException('A generic template can not have more than 10 bubbles');

        //  Set Template Type
        $this->setTemplateType( Template::TYPE_GENERIC );

        //  Set Elements
        $this->setElements( $elements );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[GENERIC_TEMPLATE]';
    }
}