<?php namespace Ayedev\Integration\Facebook\Model\Attachment\Template\Generic;

use Ayedev\Integration\Facebook\Model\Attachment\Template\AbstractElement;
use Ayedev\Integration\Facebook\Model\Button;
use Ayedev\Integration\Facebook\Model\DefaultAction;

class Element extends AbstractElement
{
    /** @var array $_fillable */
    protected $_fillable = array( 'title', 'subtitle', 'image_url', 'buttons', 'default_action' );


    /**
     * Element constructor.
     *
     * @param $title
     * @param null $subtitle
     * @param null $imageUrl
     * @param array|null $buttons
     * @param DefaultAction|null $defaultAction
     */
    public function __construct($title, $subtitle = null, $imageUrl = null, array $buttons = null, DefaultAction $defaultAction = null)
    {
        //  Setup Element
        $this->setupElement( $title, $subtitle, $imageUrl );

        //  Validate Buttons
        if( count( $buttons ) > 3 ) throw new \InvalidArgumentException( 'A generic element can not have more than 3 buttons.' );

        //  Set Buttons
        $this->setButtons( $buttons );

        //  Set Default Action
        $this->setDefaultAction( $defaultAction );
    }

    /**
     * Add Button
     *
     * @param Button $button
     * @return $this
     */
    public function addButton( Button $button )
    {
        //  Existing
        $existing = $this->getButtons();
        if( !$existing )    $existing = array();

        //  Validate Buttons
        if( count( $existing ) > 3 ) throw new \InvalidArgumentException( 'A generic element can not have more than 3 buttons.' );

        //  Append
        $existing[] = $button;

        //  Set
        $this->setButtons( $existing );

        //  Return
        return $this;
    }

    /**
     * Get First Button
     *
     * @return null
     */
    public function getFirstButton()
    {
        //  Existing
        $existing = $this->getButtons();
        if( !$existing )    $existing = array();

        //  Return
        return ( $existing && sizeof( $existing ) > 0 ? $existing[0] : null );
    }
}