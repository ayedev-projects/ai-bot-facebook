<?php namespace Ayedev\Integration\Facebook\Model\Attachment\Template;

use Ayedev\Integration\Facebook\Model\Attachment\Template;
use Ayedev\Integration\Facebook\Model\Attachment\Template\ElementList\Element;
use Ayedev\Integration\Facebook\Model\Button as ButtonModel;

class ElementList extends Template
{
    //  Types
    const TOP_STYLE_COMPACT = 'compact';
    const TOP_STYLE_LARGE = 'large';

    /** @var array $_fillable */
    protected $_fillable = array( 'template_type', 'elements', 'buttons', 'top_element_style' );


    /**
     * ElementList constructor.
     *
     * @param array $elements
     * @param ButtonModel $button
     * @param string $topElementStyle
     */
    public function __construct( array $elements, ButtonModel $button = null, $topElementStyle = self::TOP_STYLE_LARGE )
    {
        //  To Lower
        $topElementStyle = strtolower( $topElementStyle );

        //  Validate
        if( empty( $elements ) || ( count( $elements ) < 2 || count( $elements ) > 4 ) )    throw new \InvalidArgumentException('You must provide between 2 and 4 elements.');
        if( !in_array( $topElementStyle, [self::TOP_STYLE_LARGE, self::TOP_STYLE_COMPACT] ) )   throw new \InvalidArgumentException( sprintf( '"%s" is not a valid top element style.', $topElementStyle ) );
        if( $topElementStyle === self::TOP_STYLE_LARGE && empty($elements[0]->getImageUrl() ) ) throw new \InvalidArgumentException( sprintf( 'If the top element style is "%s" your first element must have an image url', self::TOP_STYLE_LARGE ) );

        //  Set Template Type
        $this->setTemplateType( Template::TYPE_LIST );

        //  Set Buttons
        $this->setButtons( [ $button ] );

        //  Set Elements
        $this->setElements( $elements );

        //  Set Elements
        $this->setTopElementStyle( $topElementStyle );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[ELEMENT_LIST_TEMPLATE]';
    }
}