<?php namespace Ayedev\Integration\Facebook\Model\Attachment\Template;

use Ayedev\Bot\Messenger\Traits\MessageTrait;

abstract class AbstractElement implements \JsonSerializable
{
    use MessageTrait;

    /** @var array $_fillable */
    protected $_fillable = array( 'title', 'subtitle', 'image_url' );


    /**
     * Setup Element
     *
     * @param string $title
     * @param null|string $subtitle
     * @param null|string $imageUrl
     */
    public function setupElement( $title, $subtitle = null, $imageUrl = null )
    {
        //  Validate
        if (mb_strlen($title) > 80) {
            throw new \InvalidArgumentException('In a element, the "title" field should not exceed 80 characters.');
        }
        if (!empty($subtitle) && mb_strlen($subtitle) > 80) {
            throw new \InvalidArgumentException('In a element, the "subtitle" field should not exceed 80 characters.');
        }

        //  Set Title
        $this->setTitle( $title );

        //  Set Subtitle
        $this->setSubtitle( $subtitle );

        //  Set Image url
        $this->setImageUrl( $imageUrl );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return 'Element: ' . $this->getTitle();
    }
}