<?php namespace Ayedev\Integration\Facebook\Model\Attachment;

use Ayedev\Integration\Facebook\Model\Attachment;

class PayloadAttachment extends Attachment
{
    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[PAYLOAD_ATTACHMENT]';
    }

    /**
     * @inheritdoc
     */
    public function toArray()
    {
        //  Return
        return [
            'attachment' => parent::toArray()
        ];
    }
}