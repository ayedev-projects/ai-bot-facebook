<?php namespace Ayedev\Integration\Facebook\Model;

use Ayedev\Bot\Messenger\Traits\MessageTrait;

class Address implements \JsonSerializable
{
    use MessageTrait;

    /** @var array $_fillable */
    protected $_fillable = array( 'street_1', 'street_2', 'postal_code', 'city', 'country', 'state' );


    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return implode( ', ', array_values( $this->toArray() ) );
    }
}