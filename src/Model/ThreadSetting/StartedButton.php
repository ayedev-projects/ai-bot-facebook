<?php namespace Ayedev\Integration\Facebook\Model\ThreadSetting;

use Ayedev\Integration\Facebook\Model\ThreadSetting;

class StartedButton implements ThreadSetting, \JsonSerializable
{
    /**
     * @var string
     */
    private $payload;


    /**
     * StartedButton constructor.
     *
     * @param $payload
     */
    public function __construct($payload)
    {
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'payload' => $this->payload,
        ];
    }
}