<?php namespace Ayedev\Integration\Facebook\Model;

use Ayedev\Bot\Messenger\Traits\MessageTrait;

class QuickReply implements \JsonSerializable
{
    use MessageTrait;

    /** @var array $_fillable */
    protected $_fillable = array( 'title', 'content_type', 'payload' );


    /**
     * Constructor
     *
     * @param string $title
     * @param string $payload
     */
    public function __construct( $title, $payload = null )
    {
        //  Setup
        $this->setupQuickReply( $title, $payload );
    }

    /**
     * @param string $title
     * @param string $payload
     */
    public function setupQuickReply( $title, $payload = null )
    {
        //  Set Title
        $this->setTitle( $title );

        //  Set Payload
        if( $payload )  $this->setPayload( $payload );

        //  Set Content Type
        $this->setContentType( 'text' );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return $this->getPayload();
    }

    /**
     * Set Title
     *
     * @param $title
     * @return string
     */
    public function setTitle( $title )
    {
        //  Check Title
        if( empty( $title ) || mb_strlen( $title ) > 20 )   throw new \InvalidArgumentException( 'QuickReply title should not exceed 20 characters.' );

        //  Return
        return $this->setValue( 'title', $title );
    }

    /**
     * Set Payload
     *
     * @param $payload
     * @return string
     */
    public function setPayload( $payload )
    {
        //  Check Payload
        if( $payload && mb_strlen( $payload ) > 1000 ) throw new \InvalidArgumentException( 'QuickReply payload should not exceed 1000 characters.' );

        //  Return
        return $this->setValue( 'payload', $payload );
    }
}