<?php namespace Ayedev\Integration\Facebook\Model;

use Ayedev\Bot\Messenger\Impl\AbstractMessage;

abstract class Attachment extends AbstractMessage
{
    //  Attachment Types
    const TYPE_AUDIO = 'audio';
    const TYPE_FILE = 'file';
    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';

    //  Template Attachment
    const TYPE_TEMPLATE = 'template';

    /** @var array $_fillable */
    protected $_fillable = array( 'type', 'payload' );


    /**
     * Constructor
     *
     * @param $type
     * @param array|Template $payload
     */
    public function __construct( $type, $payload = [] )
    {
        //  Setup
        $this->setupAttachment( $type, $payload );
    }

    /**
     * Setup Type and Payload
     *
     * @param $type
     * @param array|Template $payload
     */
    protected function setupAttachment( $type, $payload = [] )
    {
        //  Store
        $this->setType( $type );
        $this->setPayload( $payload );
    }
}