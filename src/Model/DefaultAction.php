<?php namespace Ayedev\Integration\Facebook\Model;

use Ayedev\Bot\Messenger\Traits\MessageTrait;

class DefaultAction implements \JsonSerializable
{
    use MessageTrait;

    //  Height Ratio Full
    const HEIGHT_RATIO_FULL = 'full';

    //  Height Ratio Compact
    const HEIGHT_RATIO_COMPACT = 'compact';

    //  Height Ratio Tall
    const HEIGHT_RATIO_TALL = 'tall';

    /** @var array $_fillable */
    protected $_fillable = array( 'type', 'url', 'webview_height_ratio', 'messenger_extensions', 'fallback_url' );


    /**
     * @param string $url
     */
    public function __construct( $url )
    {
        //  Set Type
        $this->setType( 'web_url' );

        //  Set URL
        $this->setUrl( $url );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return $this->getUrl();
    }

    /**
     * Set Webview Height Ratio
     *
     * @param $ratio
     * @return $this
     */
    public function setWebviewHeightRatio( $ratio )
    {
        //  Validate
        if( !in_array( $ratio, $this->getAllowedHeights() ) )   throw new \InvalidArgumentException(sprintf('Webview height ratio must be one of this values: [%s]', implode(', ', $this->getAllowedHeights())));

        //  Store
        $this->setValue( 'webview_height_ratio', $ratio );

        //  Return
        return $this;
    }

    /**
     * Get Allowed Heights
     *
     * @return string[]
     */
    private function getAllowedHeights()
    {
        return [
            self::HEIGHT_RATIO_FULL,
            self::HEIGHT_RATIO_COMPACT,
            self::HEIGHT_RATIO_TALL
        ];
    }
}