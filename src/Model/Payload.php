<?php namespace Ayedev\Integration\Facebook\Model;

use Ayedev\Bot\Messenger\Traits\MessageTrait;

class Payload implements \JsonSerializable
{
    use MessageTrait;

    /** @var array $_fillable */
    protected $_fillable = true;

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[RAW_PAYLOAD]';
    }
}