<?php namespace Ayedev\Integration\Facebook\Model;

use Ayedev\Bot\Messenger\Impl\AbstractMessage;

class Message extends AbstractMessage
{
    //  Text Message
    const TYPE_TEXT = 'text';

    //  Message with Attachment
    const TYPE_ATTACHMENT = 'attachment';

    /** @var array $_fillable */
    protected $_fillable = array( 'type', 'quick_replies', 'data' );


    /**
     * Message constructor.
     *
     * @param mixed $data
     */
    public function __construct( $data )
    {
        //  Check
        $this->setMessage( $data );

        //  Add Ignore
        $this->addIgnore( 'type' );

        //  Add Convert
        $this->addKeyConvert( 'data', function()
        {
            //  Return
            return $this->getType();
        } );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Get Data
        $data = $this->getData();

        //  Return
        return ( $data instanceof Attachment ? $data->toString() : $data );
    }

    /**
     * Set Message
     *
     * @param $data
     * @return $this
     */
    public function setMessage( $data )
    {
        //  Check for String
        if( is_string( $data ) )
        {
            //  Set Type
            $this->setValue( 'type', self::TYPE_TEXT );
        }
        elseif( $data instanceOf Attachment )
        {
            //  Set Type
            $this->setValue( 'type', self::TYPE_ATTACHMENT );
        }
        else
        {
            //  Throw Exception
            throw new \InvalidArgumentException( 'Invalid $data. It must be a string or an Attachment but ' . ( is_object( $data ) ? get_class( $data ) : ( is_null( $data ) ? 'NULL' : 'array' ) ) . ' was provided' );
        }

        //  Store Data
        $this->setValue( 'data', $data );

        //  Return
        return $this;
    }


    /**
     * Add Quick Reply
     *
     * @param QuickReply $quickReply
     * @return $this
     */
    public function addQuickReply( QuickReply $quickReply )
    {
        //  Get Existing
        $existing = $this->getQuickReplies();
        if( !$existing )    $existing = array();

        //  Check
        if( sizeof( $existing ) >= 10 )  throw new \InvalidArgumentException( 'A message can not have more than 10 quick replies.' );

        //  Add Quick Reply
        $existing[] = $quickReply;

        //  Save Again
        $this->setValue( 'quick_replies', $existing );

        //  Return
        return $this;
    }

    /**
     * Set Meta Data
     *
     * @param null|string $metadata
     * @return $this
     */
    public function setMetadata( $metadata = null )
    {
        //  Check
        if( $metadata && mb_strlen( $metadata ) > 1000 )  throw new \InvalidArgumentException( 'Metadata should not exceed 1000 characters' );

        //  Store
        $this->setValue( 'metadata', empty($metadata) ? null : $metadata );

        //  Return
        return $this;
    }
}