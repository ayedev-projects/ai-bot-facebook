<?php namespace Ayedev\Integration\Facebook\Model\Button;

use Ayedev\Integration\Facebook\Model\Button;

class Share extends Button
{
    /**
     * Share constructor.
     */
    public function __construct()
    {
        //  Set Type
        $this->setType( Button::TYPE_SHARE );
    }
}