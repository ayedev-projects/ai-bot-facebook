<?php namespace Ayedev\Integration\Facebook;

use Ayedev\Bot\Messenger\Core\ApiCall;
use Ayedev\Bot\Messenger\Impl\AbstractMessage;
use Ayedev\Bot\Messenger\Impl\AbstractMessenger;

class Service extends AbstractMessenger
{
    /**
     * Get Base Query Params
     * 
     * @return Array
     */
    protected function _baseQueryParams()
    {
        //  Query
        $query = array();

        //  Check Debug
        if( mManager()->isDebugEnabled() )
        {
            //  Enable Debug
            $query['debug'] = 'all';
        }

        //  Return
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function sendMessage( $recipient, $message, $notificationType = null )
    {
        //  Check
        if( !$notificationType )    $notificationType = Core\NotificationType::REGULAR;

        //  Create Message
        $message = $this->createMessage( $message );

        //  Generate Options for Message
        $options = Request::make( $recipient, $message, $notificationType );

        //  Run Graph
        $responseData = mManager()->getWebhook()->getApiCall()->send( 'POST', '/me/messages', null, $this->_baseQueryParams(), mManager()->getWebhook()->getHeaders(), $options )->getResponseDecoded();

        //  Return
        return new Response( $responseData['recipient_id'], $responseData['message_id'], ( isset( $responseData['attachment_id'] ) ? $responseData['attachment_id'] : null ) );
    }

    /**
     * @inheritdoc
     */
    public function getUserProfile( $userId, $fields = array() )
    {
        //  Check
        if( !$fields )
        {
            //  Assign
            $fields = [
                Model\UserProfile::FIRST_NAME,
                Model\UserProfile::LAST_NAME,
                Model\UserProfile::PROFILE_PIC,
                Model\UserProfile::LOCALE,
                Model\UserProfile::TIMEZONE,
                Model\UserProfile::GENDER,
                Model\UserProfile::PAYMENT_ENABLED,
            ];
        }

        //  Query
        $query = array_merge( $this->_baseQueryParams(), [
            'fields' => implode(',', $fields)
        ] );

        //  Get Response
        $response = mManager()->getWebhook()->getApiCall()->get( sprintf( '/%s', $userId ), $query )->getResponseDecoded();

        //  Return
        return Model\UserProfile::create( $response );
    }

    /**
     * Subscribe the app to the page
     *
     * @return bool
     */
    public function subscribe()
    {
        //  Get Response
        $response = mManager()->getWebhook()->getApiCall()->post( '/me/subscribed_apps' )->getResponseDecoded();

        //  Return
        return $response['success'];
    }

    /**
     * Set Greeting Text
     *
     * @param $text
     * @return $this
     */
    public function setGreetingText( $text )
    {
        //  Greeting Text
        $greeting = new Model\ThreadSetting\GreetingText( $text );

        //  Build Setting
        $setting = $this->buildSetting( Model\ThreadSetting::TYPE_GREETING, null, $greeting );

        //  Post Settings
        $this->postThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Delete Greeting Text
     *
     * @return $this
     */
    public function deleteGreetingText()
    {
        //  Build Setting
        $setting = $this->buildSetting( Model\ThreadSetting::TYPE_GREETING );

        //  Delete Settings
        $this->deleteThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Set Started Button
     *
     * @param string $payload
     * @return $this
     */
    public function setStartedButton( $payload )
    {
        //  Started Button
        $startedButton = new Model\ThreadSetting\StartedButton( $payload );

        //  Build Setting
        $setting = $this->buildSetting(
            Model\ThreadSetting::TYPE_CALL_TO_ACTIONS,
            Model\vThreadSetting::NEW_THREAD,
            [$startedButton]
        );

        //  Post Settings
        $this->postThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Delete Started Button
     *
     * @return $this
     */
    public function deleteStartedButton()
    {
        //  Build Setting
        $setting = $this->buildSetting(
            Model\ThreadSetting::TYPE_CALL_TO_ACTIONS,
            Model\ThreadSetting::NEW_THREAD
        );

        //  Delete Settings
        $this->deleteThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Set Persistent Menu
     *
     * @param array $menuButtons
     * @return $this
     */
    public function setPersistentMenu( array $menuButtons )
    {
        //  Check
        if( count( $menuButtons ) > 5 ) throw new \InvalidArgumentException( 'You should not set more than 5 menu items.' );

        //  Build Setting
        $setting = $this->buildSetting(
            Model\ThreadSetting::TYPE_CALL_TO_ACTIONS,
            Model\ThreadSetting::EXISTING_THREAD,
            $menuButtons
        );

        //  Post Settings
        $this->postThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Delete Persistent Menu
     *
     * @return $this
     */
    public function deletePersistentMenu()
    {
        //  Build Setting
        $setting = $this->buildSetting(
            Model\ThreadSetting::TYPE_CALL_TO_ACTIONS,
            Model\ThreadSetting::EXISTING_THREAD
        );

        //  Delete Setting
        $this->deleteThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Post Thread Settings
     *
     * @param array $setting
     * @return array
     */
    private function postThreadSettings(array $setting)
    {
        //  Return
        return mManager()->getWebhook()->getApiCall()->post( '/me/thread_settings', $setting )->getResponseDecoded();
    }

    /**
     * Delete Thread Settings
     *
     * @param array $setting
     * @return array
     */
    private function deleteThreadSettings(array $setting)
    {
        //  Return
        return mManager()->getWebhook()->getApiCall()->delete( '/me/thread_settings', $setting )->getResponseDecoded();
    }

    /**
     * Build Settings
     *
     * @param string $type
     * @param null|string $threadState
     * @param mixed $value
     *
     * @return array
     */
    private function buildSetting($type, $threadState = null, $value = null)
    {
        //  Base Settings
        $setting = [
            'setting_type' => $type,
        ];

        //  Check State
        if( !empty( $threadState ) )    $setting['thread_state'] = $threadState;

        //  Check Value
        if( !empty( $value ) )  $setting[$type] = $value;

        //  Return
        return $setting;
    }

    /**
     * Create Message
     *
     * @param string|AbstractMessage $message
     * @return AbstractMessage
     */
    private function createMessage( $message )
    {
        //  Check for Template
        if( $message instanceof Model\Attachment\Template )
        {
            //  Create Message
            $message = new Model\Attachment\PayloadAttachment( Model\Attachment\PayloadAttachment::TYPE_TEMPLATE, $message );
        }

        //  Check for String or Attachment
        if( is_string( $message ) )
        {
            //  Create Message
            return new Model\Message( $message );
        }

        //  Check for Message
        if( $message instanceof AbstractMessage )   return $message;

        //  Throw Exception
        throw new \InvalidArgumentException( 'Message should be a string, Message, Attachment or Template' );
    }
}