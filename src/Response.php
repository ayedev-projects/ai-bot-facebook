<?php namespace Ayedev\Integration\Facebook;

use Ayedev\Bot\Messenger\IFace\MessengerResponseInterface;

class Response implements MessengerResponseInterface
{
    /**
     * @var string
     */
    private $recipientId;

    /**
     * @var string
     */
    private $messageId;

    /**
     * @var string
     */
    private $attachmentId;

    /**
     * @param string $recipientId
     * @param string $messageId
     * @param $attachmentId
     */
    public function __construct( $recipientId, $messageId, $attachmentId = null )
    {
        $this->recipientId = $recipientId;
        $this->messageId = $messageId;
        $this->attachmentId = $attachmentId;
    }

    /**
     * @return string
     */
    public function getRecipientId()
    {
        return $this->recipientId;
    }

    /**
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * @return string
     */
    public function getAttachmentId()
    {
        return $this->attachmentId;
    }
}
